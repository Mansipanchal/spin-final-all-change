jQuery(document).ready(function($){
var sound = new Audio('images/tick.mp3');
	$('.spin').easyWheel({
		items: [
			{
				id      : 'a',
				name    : 'Walk When You Talk',
				message : 'Walk When You Talk',
				color   : 'url(#img1)',
				win     : true
			},{
				id      : 'b',
				name    : 'Take the stairs',
				message : 'Take the stairs',
				color   : 'url(#img2)',
				win     : true
			},{
				id      : 'c',
				name    : 'Park Farthest from the door in every parking lot',
				message : 'Park Farthest from the door in every parking lot',
				color   : 'url(#img3)',
				win     : true
			},{
				id      : 'd',
				name    : 'Choose a Walk-Friendly Compitiion',
				message : 'Choose a Walk-Friendly Compitiion',
				color   : 'url(#img4)',
				win     : false
			}
		],
		textOffset :0,
		frame: 1,
		centerLineWidth : 5,
		centerWidth : 25,
		random: true,
		textLine : 'horizontal',
  		textArc  : true,
		fontSize:5,
  		letterSpacing : 1,
  		markerColor: "#CC3333",
	    centerLineColor: "#33D0E0",
	    centerBackground: "#ffffff",
	    sliceLineColor: "#fff",	
	    outerLineColor: "#BD2E32",
	    shadowOpacity : 1,
	    centerImage : 'images/spindoc.png',
	    centerImageWidth : 25,
	    shadow : '',
		button : '.spin-to-win',
		onStart: function(results, spinCount, now) {		
			$('.spinner-message').css('color','#000');
			$('.spinner-message').html('Spinning...');
		},

		onStep: function(results,count,now) {
			
            if (typeof sound.currentTime !== 'undefined')
                sound.currentTime = 0;
            sound.play();
        },

		onComplete : function(results,count,now){
		$(document).ready(function(){
		   jQuery.noConflict();
		   $('#modal1').modal('show');
		   $('#modal1 h4').html(results.message);
					console.log(results,count,now);
					var message = $('#modal1 h4').text();
			if(message == 'Walk When You Talk'){
				$(".winimg").empty();			
			$(".winimg").append('<img src="images/when-walk.png" alt="" />')
			}
			
			if(message == 'Take the stairs'){
				$(".winimg").empty();
			$(".winimg").append('<img src="images/step-2-icon.png" alt="" />')
			}	
			if(message == 'Park Farthest from the door in every parking lot'){
				$(".winimg").empty();
			$(".winimg").append('<img src="images/step-3-icon.png" alt="" />')
			}	
			if(message == 'Choose a Walk-Friendly Compitiion'){
					$(".winimg").empty();
			$(".winimg").append('<img src="images/step-4-icon.png" alt="" />')
			}						
		});			
		//var cloneDiv = $(".spin");
		//$("#spinWrap").append('cloneDiv');
	
		}
	});
   $('#modal1 .commonBtn').on("click", function(){
	   $('#modal1').modal('hide');
	   });
	    $('#modal2 .commonBtn').on("click", function(){
	   $('#modal2').modal('hide');
	   });


});
